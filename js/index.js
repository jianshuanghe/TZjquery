
$().ready(function(e) {
   //拖拽复制体
   $('p[id^="draggable"]').draggable({
	   helper:"clone",
	   cursor: "move"
	 });

	//释放后
   $('div[id^="target"]').droppable({
		drop:function(event,ui){
			$(this).children().remove();
			var source = ui.draggable.clone();
			$('<img/>', {
				src: 'btn_delete.png',
				style:'display:none',
				click: function() {
				  source.remove();
				}
			}).appendTo(source);

		
			source.mouseenter(function () { 
				$(this).find("img").show();
			}); 

			source.mouseleave(function () {
				$(this).find("img").hide();
			}); 

			$(this).append(source);

		}
	});
 });